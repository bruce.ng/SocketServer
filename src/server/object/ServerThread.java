package server.object;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.model.Message;

public class ServerThread extends Thread {
	
	private Socket socket;
	
	public ServerThread(Socket socket) throws IOException, ClassNotFoundException {
		this.socket = socket;	
	}
	
	@Override
	public void run() {
		try {
			ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
			
			Message ms = (Message) ois.readObject();
			
			//
			if (ms != null) {
				System.out.println("Receive a message from client====");
				System.out.println("Data from client: ===============" + ms.getTitle() + "====" + ms.getBody());
				
				//to do something
				
				Message ms2 = new Message("Ebooks", "Java programming");
				ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
				oos.writeObject(ms2);
				System.out.println("Server sending to client: =======" + ms2.getTitle() + "====" + ms2.getBody());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
