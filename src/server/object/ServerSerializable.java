package server.object;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSerializable {
	
	public void connect() throws IOException, ClassNotFoundException {
		
		ServerSocket server = new ServerSocket(1989);
		
		System.out.println("Server is readying...");
		
		int count = 0;
		
		while (true) {
			System.out.println("Client: " + ++count);
			Socket socket = server.accept();
			
			ServerThread thread = new ServerThread(socket);
			thread.start();
			
		}
		
	}
	
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		ServerSerializable server = new ServerSerializable();
		server.connect();
	}
}
